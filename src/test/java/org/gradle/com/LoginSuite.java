package org.gradle.com;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginSuite {
	
	WebDriver driver;
	MethodRepo mr = new MethodRepo();
	//commented
	@BeforeMethod
	public void applaunch() throws InterruptedException
	{
		mr.browserAppLaunch();
	}

	@Test(priority = 0, enabled=true, description="TC_001: Verifying valid login functionality")
	public void verifyValidLogin() throws InterruptedException
	{
		try {
			 mr.login("cc@yopmail.com", "Qwerty@123");
			 Thread.sleep(10000);
			 mr.verifyValidLogin1();

			Thread.sleep(3000);
			}catch(Exception e) {
				System.out.println("nothing to print");
		}
	}
	
	@AfterMethod()
	public void appClose()
	{
		mr.appClose();
	}
	
}
